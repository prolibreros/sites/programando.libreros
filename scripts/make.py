import re
import sys
import json
from datetime import datetime
from pathlib import Path

root = Path(__file__).parent / ".."
about = re.sub(r'\s+', ' ', """
    Programando LIBREros es una unidad de obreros de la cultura que se dedica a
    la producción de proyectos editoriales hechos con <i>software</i> libre o
    de código abierto, al desarrollo de tecnologías libres para la edición y a
    la elaboración de propuestas jurídicas a favor del <i>software</i>, la
    cultura y la edición libres. Sobre estas líneas de trabajo, para el
    sostenimiento de esta unidad ofrecemos servicios de consultoría y de
    producción editoriales: ponte en contacto con nosotros.
""").strip()
contact = {
    "site": "https://www.programando.li/breros/",
    "gitlab": "https://gitlab.com/prolibreros",
    "mastodon": "https://mastodon.social/@proLIBREros",
    "twitter": "https://twitter.com/proLIBREros",
    "email": "hi@programando.li",
}
projects = json.loads(Path(sys.argv[1]).read_text())
data = {"acerca": about, "contacto": contact, "proyectos": projects}
index = root / "site" / "index.html"
injson = root / "site" / "prolibreros.json"
template = (root / "src" / "template.html").read_text()
body = ""

for key, val in data.items():
    if key == "contacto":
        for name, url in val.items():
            template = re.sub(f'#{name.upper()}#', url, template)
        continue
    body += f'\n<section id="{key}">'
    body += f'\n<h1>{key.capitalize()}</h1>'
    if isinstance(val, str):
        body += f'\n{val}'
    else:
        body += '<ul class="list">'
        for project in val:
            date1 = "%Y-%m-%dT%H:%M:%S.%fZ"
            date2 = "%d/%m/%Y"
            url = project["web_url"]
            created = project["created_at"]
            updated = project["last_activity_at"]
            created = datetime.strptime(created, date1).strftime(date2)
            updated = datetime.strptime(updated, date1).strftime(date2)
            body += f'\n<li class="project" id="{project["id"]}">'
            body += f'\n<h1><a class="anchor" href="#{project["id"]}">⚓</a>'
            body += f'<a class="name" href="{url}">{project["name"]}</a></h1>'
            body += '\n<p class="dates">'
            body += f'<span class="created">{created}</span>'
            body += f'<span class="updated">{updated}</span></p>'
            if project["description"]:
                url1 = r'(\S+):\s+(http\S+)'
                url2 = r'<a class="link" target="_blank" href="\2">\1</a>'
                info = re.sub(r'\s+', ' ', project["description"])
                info = re.sub(url1, url2, info)
                body += f'\n<p class="info">{info}</p>'
            body += '\n</li>'
        body += "</ul>"
    body += '\n</section>'

html = template.replace('#PROJECTS#', body.strip())
if index.read_text() != html:
    index.write_text(html)
    injson.write_text(json.dumps(data, indent=2))
