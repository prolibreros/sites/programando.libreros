JSON=projects.json

# Va a la raíz del repo
cd $(dirname $0)/..

# Descarga info de GitLab
curl --request GET \
     -s -o $JSON \
     "https://gitlab.com/api/v4/groups/6936842/projects?include_subgroups=true&with_shared=true&simple=true&per_page=100&order_by=last_activity_at"
if [ "$?" != 0 ]; then exit 1; fi

# Rehace sitio y JSON
python3 ./scripts/make.py $JSON

# Limpia repo
rm $JSON
cp -r ./src/* ./site
rm ./site/template.html
